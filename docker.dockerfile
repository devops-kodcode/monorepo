from docker:24.0.5

run apk update && \
    apk upgrade && \
    apk add yq

volume [/var/lib/docker]

entrypoint ["dockerd-entrypoint.sh"]

cmd []